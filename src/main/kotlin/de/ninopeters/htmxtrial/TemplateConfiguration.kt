package de.ninopeters.htmxtrial

import de.neuland.pug4j.PugConfiguration
import de.neuland.pug4j.spring.template.SpringTemplateLoader
import de.neuland.pug4j.spring.view.PugViewResolver
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.ViewResolver

@Configuration
class TemplateConfiguration {

    @Bean
    fun templateLoader(): SpringTemplateLoader {
        val templateLoader = SpringTemplateLoader()
        templateLoader.setTemplateLoaderPath("classpath:/templates")
        templateLoader.encoding = "UTF-8"
        templateLoader.setSuffix(".pug")
        return templateLoader
    }

    @Bean
    fun pugConfiguration(): PugConfiguration {
        val configuration = PugConfiguration()
        configuration.templateLoader = templateLoader()
        return configuration
    }

    @Bean
    fun viewResolver(): ViewResolver {
        val viewResolver = PugViewResolver()
        viewResolver.setConfiguration(pugConfiguration())
        return viewResolver
    }
}
