package de.ninopeters.htmxtrial

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.web.csrf.CsrfToken
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.view.RedirectView


@Controller
@RequestMapping
class PageController(val counterService: CounterService, val userDetailsService: UserDetailsService) {


    @GetMapping("/login")
    fun login(csrfToken: CsrfToken): ModelAndView {
        val mv = ModelAndView()
        mv.viewName = "pages/login"
        mv.model["title"] = "Login"
        mv.model["_csrf"] = csrfToken.token

        return mv
    }

    @GetMapping
    fun index(csrfToken: CsrfToken): ModelAndView {
        val mv = ModelAndView()
        mv.viewName = "pages/index"
        mv.model["title"] = "Hello World"
        mv.model["count"] = counterService.count()
        mv.model["isAdmin"] = isAdmin()
        mv.model["_csrf"] = csrfToken.token

        return mv
    }

    @GetMapping("/admin")
    fun admin(csrfToken: CsrfToken): ModelAndView {
        val mv = ModelAndView()
        mv.viewName = "pages/admin"
        mv.model["title"] = "Test"
        mv.model["count"] = counterService.count()
        mv.model["isAdmin"] = isAdmin()
        mv.model["_csrf"] = csrfToken.token

        return mv
    }

    private fun isAdmin(): Boolean {
        return SecurityContextHolder
            .getContext()
            .authentication
            .authorities
            .stream()
            .anyMatch { it.authority.equals("ROLE_ADMIN") }
    }
}
