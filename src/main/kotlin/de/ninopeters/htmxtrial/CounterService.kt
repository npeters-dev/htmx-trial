package de.ninopeters.htmxtrial

import org.springframework.stereotype.Service
import java.util.concurrent.atomic.AtomicInteger

@Service
class CounterService {

    private val count: AtomicInteger = AtomicInteger()

    fun count(): Int {
        return count.get()
    }

    fun increment(): Int {
        return count.incrementAndGet()
    }
}